Titulo
(Titulo de la solicitud de funcionalidad)

Resumen:
(Breve descripción de la funcionalidad que estás solicitando)

Descripción:
(Proporciona una explicación detallada de la funcionalidad que te gustaría agregar al proyecto)

Motivación:
(Explica por qué esta funcionalidad sería beneficiosa para el proyecto y cómo mejoraría la experiencia del usuario o el rendimiento)

Escenario de Uso:
(Ilustra un escenario típico de cómo un usuario interactuaría con esta nueva funcionalidad)

Capturas de Pantalla o Ejemplos:
(Si es aplicable, proporciona capturas de pantalla o ejemplos que ayuden a visualizar la funcionalidad solicitada)

¿Existen soluciones alternativas?
(Discute si hay formas de lograr la misma funcionalidad utilizando la configuración actual o métodos alternativos)

Información Adicional:
(Añade cualquier información adicional que creas que sea relevante)
